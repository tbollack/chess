'use strict';

(function () {
  // Games Controller Spec
  describe('Games Controller Tests', function () {
    // Initialize global variables
    var GamesController,
      scope,
      $httpBackend,
      $stateParams,
      $location,
      Authentication,
      Games,
      mockGame;

    // The $resource service augments the response object with methods for updating and deleting the resource.
    // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
    // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
    // When the toEqualData matcher compares two objects, it takes only object properties into
    // account and ignores methods.
    beforeEach(function () {
      jasmine.addMatchers({
        toEqualData: function (util, customEqualityTesters) {
          return {
            compare: function (actual, expected) {
              return {
                pass: angular.equals(actual, expected)
              };
            }
          };
        }
      });
    });

    // Then we can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_, _Authentication_, _Games_) {
      // Set a new global scope
      scope = $rootScope.$new();

      // Point global variables to injected services
      $stateParams = _$stateParams_;
      $httpBackend = _$httpBackend_;
      $location = _$location_;
      Authentication = _Authentication_;
      Games = _Games_;

      // create mock game
      mockGame = new Games({
        _id: '525a8422f6d0f87f0e407a33',
        title: 'An Game about MEAN',
        content: 'MEAN rocks!'
      });

      // Mock logged in user
      Authentication.user = {
        roles: ['user']
      };

      // Initialize the Games controller.
      GamesController = $controller('GamesController', {
        $scope: scope
      });
    }));

    it('$scope.find() should create an array with at least one game object fetched from XHR', inject(function (Games) {
      // Create a sample games array that includes the new game
      var sampleGames = [mockGame];

      // Set GET response
      $httpBackend.expectGET('api/games').respond(sampleGames);

      // Run controller functionality
      scope.find();
      $httpBackend.flush();

      // Test scope value
      expect(scope.games).toEqualData(sampleGames);
    }));

    it('$scope.findOne() should create an array with one game object fetched from XHR using a gameId URL parameter', inject(function (Games) {
      // Set the URL parameter
      $stateParams.gameId = mockGame._id;

      // Set GET response
      $httpBackend.expectGET(/api\/games\/([0-9a-fA-F]{24})$/).respond(mockGame);

      // Run controller functionality
      scope.findOne();
      $httpBackend.flush();

      // Test scope value
      expect(scope.game).toEqualData(mockGame);
    }));

    describe('$scope.craete()', function () {
      var sampleGamePostData;

      beforeEach(function () {
        // Create a sample game object
        sampleGamePostData = new Games({
          title: 'An Game about MEAN',
          content: 'MEAN rocks!'
        });

        // Fixture mock form input values
        scope.title = 'An Game about MEAN';
        scope.content = 'MEAN rocks!';

        spyOn($location, 'path');
      });

      it('should send a POST request with the form input values and then locate to new object URL', inject(function (Games) {
        // Set POST response
        $httpBackend.expectPOST('api/games', sampleGamePostData).respond(mockGame);

        // Run controller functionality
        scope.create();
        $httpBackend.flush();

        // Test form inputs are reset
        expect(scope.title).toEqual('');
        expect(scope.content).toEqual('');

        // Test URL redirection after the game was created
        expect($location.path.calls.mostRecent().args[0]).toBe('games/' + mockGame._id);
      }));

      it('should set scope.error if save error', function () {
        var errorMessage = 'this is an error message';
        $httpBackend.expectPOST('api/games', sampleGamePostData).respond(400, {
          message: errorMessage
        });

        scope.create();
        $httpBackend.flush();

        expect(scope.error).toBe(errorMessage);
      });
    });

    describe('$scope.update()', function () {
      beforeEach(function () {
        // Mock game in scope
        scope.game = mockGame;
      });

      it('should update a valid game', inject(function (Games) {
        // Set PUT response
        $httpBackend.expectPUT(/api\/games\/([0-9a-fA-F]{24})$/).respond();

        // Run controller functionality
        scope.update();
        $httpBackend.flush();

        // Test URL location to new object
        expect($location.path()).toBe('/games/' + mockGame._id);
      }));

      it('should set scope.error to error response message', inject(function (Games) {
        var errorMessage = 'error';
        $httpBackend.expectPUT(/api\/games\/([0-9a-fA-F]{24})$/).respond(400, {
          message: errorMessage
        });

        scope.update();
        $httpBackend.flush();

        expect(scope.error).toBe(errorMessage);
      }));
    });

    describe('$scope.remove(game)', function () {
      beforeEach(function () {
        // Create new games array and include the game
        scope.games = [mockGame, {}];

        // Set expected DELETE response
        $httpBackend.expectDELETE(/api\/games\/([0-9a-fA-F]{24})$/).respond(204);

        // Run controller functionality
        scope.remove(mockGame);
      });

      it('should send a DELETE request with a valid gameId and remove the game from the scope', inject(function (Games) {
        expect(scope.games.length).toBe(1);
      }));
    });

    describe('scope.remove()', function () {
      beforeEach(function () {
        spyOn($location, 'path');
        scope.game = mockGame;

        $httpBackend.expectDELETE(/api\/games\/([0-9a-fA-F]{24})$/).respond(204);

        scope.remove();
        $httpBackend.flush();
      });

      it('should redirect to games', function () {
        expect($location.path).toHaveBeenCalledWith('games');
      });
    });
  });
}());
