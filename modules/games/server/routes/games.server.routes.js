'use strict';

/**
 * Module dependencies.
 */
var gamesPolicy = require('../policies/games.server.policy'),
  games = require('../controllers/games.server.controller');

module.exports = function (app) {
  // Games collection routes
  app.route('/api/games').all(gamesPolicy.isAllowed)
    .get(games.list)
    .post(games.create);

  // Single game routes
  app.route('/api/games/:gameId').all(gamesPolicy.isAllowed)
    .get(games.read)
    .put(games.update)
    .delete(games.delete);

  // Finish by binding the game middleware
  app.param('gameId', games.gameByID);
};
