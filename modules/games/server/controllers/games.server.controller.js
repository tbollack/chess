'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Game = mongoose.model('Game'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a game
 */
exports.create = function (req, res) {
  var game = new Game(req.body);
  game.user = req.user;

  game.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(game);
    }
  });
};

/**
 * Show the current game
 */
exports.read = function (req, res) {
  res.json(req.game);
};

/**
 * Update a game
 */
exports.update = function (req, res) {
  var game = req.game;

  game.title = req.body.title;
  game.content = req.body.content;
  game.data = req.body.data;

  game.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(game);
    }
  });
};

/**
 * Delete an game
 */
exports.delete = function (req, res) {
  var game = req.game;

  game.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(game);
    }
  });
};

/**
 * List of Games
 */
exports.list = function (req, res) {
  Game.find().sort('-created').populate('user', 'displayName').exec(function (err, games) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(games);
    }
  });
};

/**
 * Game middleware
 */
exports.gameByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Game is invalid'
    });
  }

  Game.findById(id).populate('user', 'displayName').exec(function (err, game) {
    if (err) {
      return next(err);
    } else if (!game) {
      return res.status(404).send({
        message: 'No game with that identifier has been found'
      });
    }
    req.game = game;
    next();
  });
};
