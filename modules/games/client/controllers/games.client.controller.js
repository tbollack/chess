'use strict';

// Games controller

angular.module('games').controller('GamesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Games',
  function ($scope, $stateParams, $location, Authentication, Games) {
    $scope.authentication = Authentication;

    $scope.newInstance = function(clsDef, objInit){
      var objReturn = {};
      angular.copy(clsDef, objReturn);

      if(objInit){
        objReturn = angular.merge(objReturn, objInit);
      }

      return objReturn;

    };

    /*
     define _const objects
     */
    $scope._const = {
      created: -1,
      white: 0,
      black: 1,
      both: 2,
      color: [
        {id: 0, value: 'White'},
        {id: 1, value: 'Black'},
        {id: 2, value: 'Both'}
      ],
      side: -1,
      turn: 0,
      iAm: 'the side / color template'
    };
    console.log('$scope._const',$scope._const);


    /*
     define _pref objects
     */
    $scope._pref = {
      created: new Date().getTime(),
      side: Math.floor(Math.random() * 3)
    };
    console.log('$scope._pref',$scope._pref);


    /*
     define vector object
     */
    $scope._const.vector = {
      // one per side
      path: [
        {
          nn:   {x:  0, y:  2},
          n:    {x:  0, y:  1},
          ne:   {x:  1, y:  1},
          e:    {x:  1, y:  0},
          se:   {x:  1, y: -1},
          s:    {x:  0, y: -1},
          sw:   {x: -1, y: -1},
          w:    {x: -1, y:  0},
          nw:   {x: -1, y:  1},
          nne:  {x:  1, y:  2},
          ene:  {x:  2, y:  1},
          ese:  {x:  2, y: -1},
          sse:  {x:  1, y: -2},
          ssw:  {x: -1, y: -2},
          wsw:  {x: -2, y: -1},
          wnw:  {x: -2, y:  1},
          nnw:  {x: -1, y:  2}
        },
        {
          nn:   {x:  0, y: -2},
          n:    {x:  0, y: -1},
          ne:   {x: -1, y: -1},
          e:    {x: -1, y:  0},
          se:   {x: -1, y:  1},
          s:    {x:  0, y:  1},
          sw:   {x:  1, y:  1},
          w:    {x:  1, y:  0},
          nw:   {x:  1, y: -1},
          nne:  {x: -1, y: -2},
          ene:  {x: -2, y: -1},
          ese:  {x: -2, y:  1},
          sse:  {x: -1, y:  2},
          ssw:  {x:  1, y:  2},
          wsw:  {x:  2, y:  1},
          wnw:  {x:  2, y: -1},
          nnw:  {x:  1, y: -2}
        }
      ],
      // actual per piece paths, to be populated later
      paths: []
    };

    $scope.getVectors = function(arrVectors, intSide){
      var arrReturn = [];

      arrVectors.forEach(function(vector){
        arrReturn.push($scope._const.vector.path[intSide][vector]);
      });

      return arrReturn;
    };

    /*
     define the basic piece objects
     all pieces have types, weights, and other low-level structures
     */
    $scope._const.piece = {
      side: -1,
      color: '',
      type: '',
      weight: -1.0,
      pointsRatio: 100,
      points: 0,
      position: {x: -1, y: -1},
      vectors: [],
      canSlide: false
    };

    $scope._pref.pieces = []; // to become $scope._const.pieces[side]; _pref because weights

    $scope._const.pieceTypes = {};
    $scope._pref.pieceTypes = {};

    $scope._pref.pieceTypes.pawn = {
      type: 'Pawn',
      weight: 1.0
    };

    $scope._pref.pieceTypes.rook = {
      type: 'Rook',
      weight: 5.2
    };

    $scope._pref.pieceTypes.knight = {
      type: 'Knight',
      weight: 3.1
    };

    $scope._pref.pieceTypes.bishop = {
      type: 'Bishop',
      weight: 3.2
    };

    $scope._pref.pieceTypes.queen = {
      type: 'Queen',
      weight: 9.6
    };

    $scope._pref.pieceTypes.king = {
      type: 'King',
      weight: 10000 // effectively, the king is infinitely valuable
    };







    // !!!!: best intentions, and some of the math might be fun to keep around, but this is wrong.
    // TODO: work on board, FEN in & out, and console.log board representation next - generate pieces from board
/*
    for (var intSideIndex = $scope._const.white; intSideIndex <= $scope._const.black; intSideIndex++){
      // gross outer loop, only runs twice

      var arrPieces = []; // init the piece array for this side

      // run through pawns
      for (var intPawnIndex = 0; intPawnIndex < 8; intPawnIndex++){
        var objPawn = $scope.newInstance($scope._const.piece, $scope._pref.pieceTypes.pawn);

        objPawn = angular.merge(objPawn, {
          side: intSideIndex,
          color: $scope._const.color[intSideIndex].value,
          position: {x: intPawnIndex, y: 1 + (intSideIndex * 5)}
        });

        objPawn.points = objPawn.weight * objPawn.pointsRatio;

        objPawn.vectors = $scope.getVectors(['nn','n','nw','ne'], intSideIndex);




        arrPieces.push(objPawn);
      }




      $scope._pref.pieces.push(arrPieces); // push the array of pieces onto the side

    }
*/

      /*
       define specific pawn and pieces, inheriting from generic piece object above
       all pawns have isEnPassant flag, initialized to false
       all pawns have canMove2Squares flag, initialized to true
       all rooks have canCastle flag, initialized to true
       all queens have specific point-value weight (9.5)
       */


    /*
     define board container
     */


    /*
     define game container based on _const and _pref objects
     */
    $scope.assignPrefs = function(){
      $scope._pref.iAm = $scope._const.color[$scope._pref.side].value;

      $scope.Game = {
        _const: angular.merge($scope._const, $scope._pref)
      };
      console.log('$scope.Game',$scope.Game);
    };
    $scope.assignPrefs();

    // Create new Game
    $scope.create = function () {
      // Create new Game object
      var game = new Games({
        title: this.title,
        content: this.content,
        data: $scope.Game
      });

      // Redirect after save
      game.$save(function (response) {
        $location.path('games/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Game
    $scope.remove = function (game) {
      if (game) {
        game.$remove();

        for (var i in $scope.games) {
          if ($scope.games[i] === game) {
            $scope.games.splice(i, 1);
          }
        }
      } else {
        $scope.game.$remove(function () {
          $location.path('games');
        });
      }
    };

    // Update existing Game
    $scope.update = function () {
      var game = $scope.game;

      game.$update(function () {
        $location.path('games/' + game._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Games
    $scope.find = function () {
      $scope.games = Games.query();
    };

    // Find existing Game
    $scope.findOne = function () {
      $scope.game = Games.get({
        gameId: $stateParams.gameId
      });
    };
  }
]);
