'use strict';

// Configuring the Games module
angular.module('games').run(['Menus',
  function (Menus) {
    // Add the games dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Games',
      state: 'games',
      type: 'dropdown',
      position: 1
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'games', {
      title: 'List Games',
      state: 'games.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'games', {
      title: 'Create Games',
      state: 'games.create'
    });
  }
]);
